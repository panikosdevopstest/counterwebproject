package com.qaagility.controller;

public class Count {

    public int div(int first, int second) {
        if (second == 0) {
            return Integer.MAX_VALUE;
        }
        else {
            return first / second;
        }
    }

}
