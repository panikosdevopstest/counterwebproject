package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class CountTest {
    @Test
    public void testDiv()  throws Exception {

	int k = new Count().div(10,2);
	assertEquals( 5.0, k , 0.1);

	}

	@Test
    public void testDivZero()  throws Exception {

	int k = new Count().div(10,0);
	assertEquals( "Inf test" , k , Integer.MAX_VALUE);

	}

}
